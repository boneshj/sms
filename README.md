# README

This is a cut down part of a larger application, used to make and track appointments in a health clinic.  This section is used to send SMS 
confirmations to clients 2 days out from their appointment.  They then reply by a 'Yes' ot a 'No' to confirm or cancel the appointment.

Twilio is used to send and receive messages.  *Note* that the Twilio credentials are not included in this code!

Only those clients registered to be contacted by SMS and have a mobile (cell) phone as their primary phone will receive a message.  
Each clinic can be selectively turned off an on for SMS notifications.

The system is written for Australian conditions and tests to make sure that mobile numbers conform to the Australian standard.  Clinics can 
be in differenttime zones, thus the system works out the appointment time based on that time zone, and takes in to account Daylight Saving 
Time.  

Clients can book more than one appointment for a day, only one notification is sent out at a time.  A subsequent notification will be sent 
only once the initial notification is replied to.  This solves the problem of trying to sort out which notification a client is replying 
to, as SMS do not have a 'conversation' system like email does.

## Rake File
The rake file, sms.rake, consists of 'notify', 'acknowledgment' and 'expire'.
### rake sms:notify
This runs through all locations with SMS notifications turned on, sets the time zone, finds all appointments in the next 2 days and sends
the reminders.
### rake sms:acknowledgment
Finds all SMS replies and tries to match then up to the appointments.
### rake sms:expire
Expires all SMS messages that have not been replied to once the booking has passed.  This means future messages can be sent to that client.

## Models
* Location
* Client
* Treatment
* Booking
* ClientPhone
* ConfirmationMessage

## SmsReminderService
The code is found in ../app/services/sms_reminder_service.rb.  This is the heart of the system for sending and recieving the 
acknowledgements.

## Testing
Run the Rspec test in ../spec/services/sms_reminder_service_spec.rb.  Note that there are no specs for the models, except for ConfirmationMessage, as for the most part 
they are placeholders.
Run the test in ../spec/models/confirmation_message_spec.rb which tests the expiry functionality.

## Installation
Assumes Ruby 2.3+ installed.  

* Clone to your favourite place  

* run `bundle install`

* run `rake db:create`

* run `rake db:migrate`

* run the tests

* Note that trying to run `rake sms:notify` and `sms:acknowledgment` will fail as the Twilio credentials have not been set up