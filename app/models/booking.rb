class Booking < ApplicationRecord
  belongs_to :client
  belongs_to :treatment
  belongs_to :location
  has_one :confirmation_message

  def expired_reservation?
    reservation_expiry_date_and_time.present? && reservation_expiry_date_and_time < Time.now.utc
  end

  def cancel
    self.status = 'Cancelled'

    save(validate: false)
    ## Other code here...

    self
  end

  def confirm
    self.status = 'Confirmed'
    save!
    ## Other code here...

    self
  end

  def client_sms_reminder
    Client.sms_reminder(client_id)
  end
end
