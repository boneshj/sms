class Client < ApplicationRecord
  belongs_to :location
  has_many :bookings
  has_many :client_phones
  has_one :primary_client_phone, -> { where('client_phones.primary IS TRUE') }, class_name: 'ClientPhone'
  has_one :primary_mobile_phone, -> { where("client_phones.primary IS TRUE and client_phones.number_type = 'Mobile'") }, class_name: 'ClientPhone'

  def self.sms_reminder(id)
    find_by(id: id, confirm_by: 'SMS')
  end

  def phone
    primary_client_phone.try(:number)
  end

  def mobile_phone
    primary_mobile_phone.try(:number)
  end
end
