class ConfirmationMessage < ApplicationRecord
  belongs_to :booking

  def expire
    do_expiry if booking.start_date_and_time.to_date < Time.parse(DateTime.now.to_s).getutc.to_date
  end

  def self.ignored
    where("responses_received is null OR LOWER(responses_received) not in ('yes', 'no', 'expired')")
  end

  def self.filters(params)
    confirmation_messages = ConfirmationMessage.all.includes(booking: [:location, :client]).where('locations.sms_ready')
    confirmation_messages = locations(confirmation_messages, params)
    confirmation_messages = confirmation_messages.ignored if params[:is_ignored].present? && params[:is_ignored] == 'true'
    confirmation_messages = sent_from_filter(confirmation_messages, params)
    confirmation_messages = sent_to_filter(confirmation_messages, params)
    confirmation_messages
  end

  def self.locations(confirmation_messages, params)
    params.delete(:location) if params[:location] == '? number:NaN ?'
    confirmation_messages = confirmation_messages.joins(booking: :location)
    confirmation_messages = confirmation_messages.where('locations.name = ?', params[:location]) if params[:location].present?
    confirmation_messages
  end

  def self.sent_from_filter(confirmation_messages, params)
    confirmation_messages = confirmation_messages.where('sent_datetime >= ?', Date.parse(params[:sent_from]).beginning_of_day) if params[:sent_from].present?
    confirmation_messages
  end

  def self.sent_to_filter(confirmation_messages, params)
    confirmation_messages = confirmation_messages.where('sent_datetime <= ?', Date.parse(params[:sent_to]).end_of_day) if params[:sent_to].present?
    confirmation_messages
  end

  private

  def do_expiry
    self.responses_received = 'Expired'
    self.actions_taken = "Expired on #{DateTime.now.to_date}"
    save
  end
end
