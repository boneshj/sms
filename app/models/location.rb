class Location < ApplicationRecord
  has_many :clients
  has_many :bookings

  def self.sms_ready
    where(sms_ready: true)
  end
end
