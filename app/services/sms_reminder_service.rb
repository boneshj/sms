# Service for reminders
class SmsReminderService
  attr_accessor :confirm_message

  SID = Rails.application.secrets[:twilio_account_sid]
  TOKEN = Rails.application.secrets[:twilio_auth_token]
  TW_NUMBER = Rails.application.secrets[:twilio_phone_number]

  def initialize
    @twilio_client = Twilio::REST::Client.new(SID, TOKEN)
  end

  def confirm_messages(list_date)
    api_list_messages(list_date).each do |message|
      next unless continue_message_update?(message['from'], message['sid'], message['status'])
      set_confirmation_status(message['sid'], confirmation_message(message['from']), message['body'], get_confirmation_status(message['body']))
    end
  end

  def remind(appointment)
    mobile_phone = mobile_phone?(appointment)
    return unless mobile_phone.present?
    return if ConfirmationMessage.find_by(mobile: mobile_phone, responses_received: nil).present?
    send_msg(mobile_phone, msg_text(appointment))
    create_confirmation_message(appointment, mobile_phone)
    update_appointment(appointment)
  end

  private

  def message_received?(mobile, status)
    "+#{TW_NUMBER}" != mobile && status == 'received'
  end

  def continue_message_update?(mobile, sid, status)
    return false unless message_received?(mobile, status)
    return false if ConfirmationMessage.find_by(mobile: mobile, sms_id: sid).present?
    return false unless confirmation_message(mobile).present?
    true
  end

  def confirmation_message(mobile)
    @confirm_message = ConfirmationMessage.where(mobile: mobile, responses_received: nil).order(:created_at).first
  end

  def api_list_messages(list_date)
    uri = URI.parse("https://api.twilio.com/2010-04-01/Accounts/#{SID}/Messages.json?DateSent>=#{list_date}")
    request = Net::HTTP::Get.new(uri)
    request.basic_auth(SID, TOKEN)

    messages = JSON.parse((Net::HTTP.start(uri.hostname, uri.port, req_options(uri)) { |http| http.request(request) }).body.gsub('=>', ':'))['messages']
    messages.present? ? messages : {}
  end

  def req_options(uri)
    { use_ssl: uri.scheme == 'https' }
  end

  def msg_text(appointment)
    date = appointment.start_date_and_time
    "Hi, \n Wellnation Clinics #{appointment.location.name} is confirming your attendance for #{appointment.treatment.name}: #{(date - Time.zone.utc_offset).strftime('%d-%b-%Y')} @ " \
    "#{(date - Time.zone.utc_offset).strftime('%I:%M %p')}. " \
    "Please arrive 5 mins prior to appt. \n\n REPLY: YES or NO. NOTE: you can only reply once, if you need to change your response, please call the clinic on " \
    "1300 859 785. \n\n Online bookings are available at www.wellnationclinics.com.au. \n\n Wellnation clinics are cashless clinics."
  end

  def send_msg(mobile_phone, message)
    @twilio_client.account.messages.create(from: TW_NUMBER, to: mobile_phone, body: message)
  end

  def update_appointment(appointment)
    appointment.update(confirmation_status: 1)
  end

  def create_confirmation_message(appointment, mobile_phone)
    ConfirmationMessage.create(booking_id: appointment.id, message_text: msg_text(appointment),
                               sent_datetime: DateTime.now, mobile: mobile_phone)
  end

  def get_confirmation_status(body)
    if %w(YES Y).include?(body.upcase)
      true
    elsif %w(NO N).include?(body.upcase)
      false
    end
  end

  def set_confirmation_status(sid, confirmation_message, message, status)
    ActiveRecord::Base.transaction do
      update_booking_confirmation_status(confirmation_message.booking, status)
      update_confirmation_message_responses_received(sid, confirmation_message, message)
      update_confirmation_message_actions_taken(confirmation_message, status)
    end
  rescue StandardError => error
    Rails.logger.info "SMS: #{confirmation_message.mobile}: #{error}"
    send_msg(mobile_phone?(confirmation_message.booking), "Cannot 'Cancel' the appointment, please call the clinic.")
  end

  def update_booking_confirmation_status(booking, status)
    return if status.nil?
    if status
      booking.confirm
    else
      booking.cancel
    end
  end

  def update_confirmation_message_responses_received(sid, confirmation_message, response)
    confirmation_message.responses_received = response
    confirmation_message.sms_id = sid
    confirmation_message.save
  end

  def update_confirmation_message_actions_taken(confirmation_message, status)
    return unless status.nil? || status || !status

    confirmation_message.update(actions_taken: action_taken(status))
    confirmation_message.booking.update(confirmation_status: status ? 1 : 0)
  end

  def action_taken(status)
    { true: 'Confirmed Appointment', false: 'Confirmed Appointment' }.fetch(status ? :true : :false, 'None')
  end

  def mobile_phone?(appointment)
    primary_mobile = appointment.client_sms_reminder.try(:mobile_phone)
    if primary_mobile.present?
      primary_mobile[0] = '+61' if primary_mobile[0] == '0'
    end
    primary_mobile
  end
end
