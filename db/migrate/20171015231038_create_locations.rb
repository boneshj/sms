class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.string :name
      t.boolean :sms_ready
      t.string :timezone

      t.timestamps
    end
  end
end
