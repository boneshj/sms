class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.references :location, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :confirm_by

      t.timestamps
    end
  end
end
