class CreateBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings do |t|
      t.references :client, foreign_key: true
      t.references :treatment, foreign_key: true
      t.references :location, foreign_key: true
      t.datetime :start_date_and_time
      t.string :status
      t.boolean :confirmation_status

      t.timestamps
    end
  end
end
