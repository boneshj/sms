class CreateConfirmationMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :confirmation_messages do |t|
      t.text :message_text
      t.datetime :scheduled_sending_datetime
      t.datetime :sent_datetime
      t.text :responses_received
      t.string :actions_taken
      t.references :booking, foreign_key: true
      t.string :mobile
      t.string :sms_id

      t.timestamps
    end
  end
end
