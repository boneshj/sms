namespace :sms do
  desc 'Find upcoming appointments and notify clients via sms '
  task notify: [:environment] do
    Location.sms_ready.each do |location|
      Time.zone = location.timezone
      Booking.where("(confirmation_status is null or not confirmation_status) and start_date_and_time < '#{Time.now.in_time_zone + 2.days}' " \
                    "and status = 'Booked' and location_id = ?", location.id).each do |booking|
        next if booking.expired_reservation?
        SmsReminderService.new.remind(booking)
      end
    end
  end

  desc 'Find acknowledgment sms '
  task acknowledgment: [:environment] do
    SmsReminderService.new.confirm_messages(DateTime.current.to_date - 1.days)
  end

  desc 'Expire confirmation_messages after the booking date and time has passed'
  task expire: [:environment] do
    ConfirmationMessage.where(responses_received: nil).each(&:expire)
  end
end
