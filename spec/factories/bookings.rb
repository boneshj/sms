FactoryGirl.define do
  factory :booking do
    client
    treatment
    location
    start_date_and_time { Faker::Time.forward(23, :all) }
    status { 'Booked' }
    confirmation_status { 0 }
  end
end
