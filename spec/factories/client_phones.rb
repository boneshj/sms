FactoryGirl.define do
  factory :client_phone do
    number { Faker::Base.numerify('+614########') }
    number_type { 'Mobile' }
    client
    primary 1
  end
end
