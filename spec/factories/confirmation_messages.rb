FactoryGirl.define do
  factory :confirmation_message do
    scheduled_sending_datetime { DateTime.now }
    sent_datetime { DateTime.now }
    booking { FactoryGirl.create(:booking) }
    mobile { self.booking.client_sms_reminder.mobile_phone }
    message_text { "Wellnation Clinics #{self.booking.location.name} confirming appt: #{ (DateTime.now + 2.days).strftime('%F')} @ #{DateTime.now.strftime('%H:%M') }. " \
      'Arrive 5 mins prior to appt. REPLY: YES or NO. NOTE: you can only reply once, if you need to change your response, please call the clinic.' }
  end
end
