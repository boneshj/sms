FactoryGirl.define do
  factory :location do
    name { Faker::Address.city }
    timezone { Faker::Address.time_zone }
    sms_ready { 1 }
  end
end
