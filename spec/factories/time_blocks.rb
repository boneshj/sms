FactoryGirl.define do
  factory :time_block do
    clinic_session
    location
    room
    start_date_and_time { Time.zone.now + rand(100).days }
    available true
  end
end
