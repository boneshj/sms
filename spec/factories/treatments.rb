FactoryGirl.define do
  factory :treatment do
    name { Faker::Ancient.primordial }
  end
end
