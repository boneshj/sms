require 'rails_helper'

describe ConfirmationMessage do
  describe 'association' do
    it { should belong_to(:booking) }
  end

  describe '.expire' do
    let!(:location) { create(:location) }
    let(:expired_booking) { create(:booking, location: location) }

    before :each do
      @confirmation_message = FactoryGirl.create(:confirmation_message, booking: expired_booking)
    end

    it 'will expire if the booking date is passed' do
      expired_booking.start_date_and_time = DateTime.now - 1.days
      expired_booking.save
      @confirmation_message.expire
      expect(@confirmation_message.responses_received).to eq('Expired')
      expect(@confirmation_message.actions_taken).to eq("Expired on #{Date.today}")
    end

    it 'will do nothing if the booking date is after today' do
      expired_booking.start_date_and_time = DateTime.now + 1.days
      expired_booking.save
      @confirmation_message.expire
      expect(@confirmation_message.responses_received).to be_nil
      expect(@confirmation_message.actions_taken).to be_nil
    end

    it 'will do nothing if the booking date is today' do
      expired_booking.start_date_and_time = Time.now
      expired_booking.save
      @confirmation_message.expire
      expect(@confirmation_message.responses_received).to be_nil
      expect(@confirmation_message.actions_taken).to be_nil
    end
  end

  context '.self' do
    describe '.ignored' do
      let!(:all_confirmation_messages) { create_list(:confirmation_message, 4) }

      it 'will return 4 confirmation_messages' do
        expect(ConfirmationMessage.ignored.count).to eq(4)
      end

      it 'will return 3 confirmation_messages if one is set to a response' do
        ConfirmationMessage.all.first.update(responses_received: 'Yes', actions_taken: 'Confirmed', sms_id: Faker::Crypto.md5)
        expect(ConfirmationMessage.ignored.count).to eq(3)
      end

      it 'will return 3 confirmation_messages if one is set expired' do
        message = ConfirmationMessage.all.to_a.sample
        message.booking.update(start_date_and_time: DateTime.now - 1.days)
        message.expire
        expect(ConfirmationMessage.ignored.count).to eq(3)
      end
    end

    describe '.filters' do
      let(:location) { create(:location, sms_ready: true) }
      let!(:bookings) { create_list(:booking, 4, location: location )}

      before :each do
        bookings.each do |booking|
          create(:confirmation_message, booking: booking)
        end
      end

      context 'no parameters are set' do
        it 'retrieves 4 confirmation messages' do
          expect(ConfirmationMessage.filters({}).count).to eq(4)
        end

        it 'retrieves 3 confirmation messages when one booking has a location with sms turned off' do
          bookings.to_a.sample.update(location: create(:location, sms_ready: false))
          expect(ConfirmationMessage.filters({}).count).to eq(3)
        end
      end

      context 'location parameter is set' do
        describe "location parameter is passed as '? number:NaN ?'" do
          it 'retrieves 4 confirmation messages' do
            expect(ConfirmationMessage.filters(location: '? number:NaN ?').count).to eq(4)
          end

          it 'retrieves 3 confirmation messages when one booking has a location with sms turned off' do
            bookings.to_a.sample.update(location: create(:location, sms_ready: false))
            expect(ConfirmationMessage.filters(location: '? number:NaN ?').count).to eq(3)
          end

          it 'retrieves 4 confirmation messages when one booking has a different location with sms turned on' do
            bookings.to_a.sample.update(location: create(:location, sms_ready: true))
            expect(ConfirmationMessage.filters(location: '? number:NaN ?').count).to eq(4)
          end
        end

        describe 'location parameter is passed as name' do
          before :each do
            @location_name = bookings.map { |x| x.location.name }.uniq[0]
          end

          it 'retrieves 4 confirmation messages' do
            expect(ConfirmationMessage.filters(location: @location_name).count).to eq(4)
          end

          it 'retrieves 3 confirmation messages when one booking has a location with sms turned off' do
            bookings.to_a.sample.update(location: create(:location, sms_ready: false))
            expect(ConfirmationMessage.filters(location: @location_name).count).to eq(3)
          end

          it 'retrieves 3 confirmation messages when one booking has a different location' do
            bookings.to_a.sample.update(location: create(:location, sms_ready: true))
            expect(ConfirmationMessage.filters(location: @location_name).count).to eq(3)
          end

          it 'retrieves 0 confirmation messages when an unknown location name is passed' do
            expect(ConfirmationMessage.filters(location: 'unknown').count).to eq(0)
          end
        end
      end

      context 'is_ignored parameter is set' do
        it 'retrieves 4 confirmation messages when is_ignored is true' do
          expect(ConfirmationMessage.filters(is_ignored: 'true').count).to eq(4)
        end

        it 'retrieves 3 confirmation messages when is_ignored is true and 1 confirmation message has been set' do
          ConfirmationMessage.second.update(responses_received: 'Yes', actions_taken: 'Confirmed', sms_id: Faker::Crypto.sha1)
          expect(ConfirmationMessage.filters(is_ignored: 'true').count).to eq(3)
        end

        it 'retrieves 4 confirmation messages when is_ignored is false and 1 confirmation message has been set' do
          ConfirmationMessage.all.to_a.sample.update(responses_received: 'Yes', actions_taken: 'Confirmed', sms_id: Faker::Crypto.sha1)
          expect(ConfirmationMessage.filters(is_ignored: 'false').count).to eq(4)
        end

        it 'retrieves 4 confirmation messages when is_ignored is false' do
          expect(ConfirmationMessage.filters(is_ignored: false).count).to eq(4)
        end

        it 'retrieves 0 confirmation messages when is_ignored is true and a unknown location is passed' do
          expect(ConfirmationMessage.filters(is_ignored: 'true', location: 'unknown').count).to eq(0)
        end

        it 'retrieves 4 confirmation messages when is_ignored is true and a know location name is passed' do
          expect(ConfirmationMessage.filters(is_ignored: 'true', location: bookings.map { |x| x.location.name }.uniq[0]).count).to eq(4)
        end

        it 'retrieves 2 confirmation messages when is_ignored is true and 1 confirmation message has been set and one booking is at a different location' do
          ConfirmationMessage.all.to_a.sample.update(responses_received: 'Yes', actions_taken: 'Confirmed', sms_id: Faker::Crypto.sha1)
          ConfirmationMessage.where(responses_received: nil).to_a.sample.booking.update(location: create(:location, sms_ready: true))
          expect(ConfirmationMessage.filters(is_ignored: 'true', location: bookings.map { |x| x.location.name }.uniq[0]).count).to eq(2)
        end
      end

      context 'sent_from is set' do
        before :each do
          ConfirmationMessage.update_all(sent_datetime: DateTime.now)
        end

        it 'retrieves 4 if date is prior to today' do
          expect(ConfirmationMessage.filters(sent_from: (DateTime.now - 1.days).strftime('%Y-%m-%d')).count).to eq(4)
        end

        it 'retrieves 4 if date is today' do
          expect(ConfirmationMessage.filters(sent_from: DateTime.now.strftime('%Y-%m-%d')).count).to eq(4)
        end

        it 'retrieves 0 if date is after today' do
          expect(ConfirmationMessage.filters(sent_from: (DateTime.now + 1.days).strftime('%Y-%m-%d')).count).to eq(0)
        end

        it 'retrieves 3 if one date is before the filter' do
          ConfirmationMessage.all.to_a.sample.update(sent_datetime: DateTime.now - 2.days)
          expect(ConfirmationMessage.filters(sent_from: (DateTime.now - 1.days).strftime('%Y-%m-%d')).count).to eq(3)
        end

        it 'retrieves 0 confirmation messages when filter is today and a unknown location is passed' do
          expect(ConfirmationMessage.filters(set_from: DateTime.now.strftime('%Y-%m-%d'), location: 'unknown').count).to eq(0)
        end

        it 'retrieves 4 confirmation messages when filter is today and a known location is passed' do
          expect(ConfirmationMessage.filters(set_from: DateTime.now.strftime('%Y-%m-%d'), location: bookings.map { |x| x.location.name }
              .uniq[0]).count).to eq(4)
        end
      end

      context 'sent_to is set' do
        before :each do
          ConfirmationMessage.update_all(sent_datetime: DateTime.now + (Time.now.utc_offset / 3600).hours)
        end

        it 'retrieves 0 if date is prior to today' do
          expect(ConfirmationMessage.filters(sent_to: (DateTime.now - 1.days).strftime('%Y-%m-%d')).count).to eq(0)
        end

        it 'retrieves 4 if date is today' do
          expect(ConfirmationMessage.filters(sent_to: DateTime.now.strftime('%Y-%m-%d')).count).to eq(4)
        end

        it 'retrieves 4 if date is after today' do
          expect(ConfirmationMessage.filters(sent_to: (DateTime.now + 1.days).strftime('%Y-%m-%d')).count).to eq(4)
        end

        it 'retrieves 1 if one date is before the filter' do
          ConfirmationMessage.all.to_a.sample.update(sent_datetime: (DateTime.now - 2.days) + (Time.now.utc_offset / 3600).hours)
          expect(ConfirmationMessage.filters(sent_to: (DateTime.now - 1.days).strftime('%Y-%m-%d')).count).to eq(1)
        end

        it 'retrieves 0 confirmation messages when filter is today and a unknown location is passed' do
          expect(ConfirmationMessage.filters(set_to: DateTime.now.strftime('%Y-%m-%d'), location: 'unknown').count).to eq(0)
        end

        it 'retrieves 4 confirmation messages when filter is today and a known location is passed' do
          expect(ConfirmationMessage.filters(set_to: DateTime.now.strftime('%Y-%m-%d'), location: bookings.map { |x| x.location.name }
                                                                                                        .uniq[0]).count).to eq(4)
        end

        it 'retrieves 3 confirmation messages when filter is today and a known location is passed, but one booking is a different location' do
          ConfirmationMessage.where(responses_received: nil).to_a.sample.booking.update(location: create(:location, sms_ready: true))
          expect(ConfirmationMessage.filters(set_to: DateTime.now.strftime('%Y-%m-%d'), location: bookings.map { |x| x.location.name }
                                                                                                        .uniq[0]).count).to eq(3)
        end
      end

      context 'both sent_from and sent_to is set' do
        before :each do
          ConfirmationMessage.update_all(sent_datetime: DateTime.now + (Time.now.utc_offset / 3600).hours)
        end

        it 'returns 4 confirmation_messages when the sent datetime is between both dates' do
          expect(ConfirmationMessage.filters(sent_from: (DateTime.now - 1.days).strftime('%Y-%m-%d'),
                                             sent_to: (DateTime.now + 1.days).strftime('%Y-%m-%d')).count).to eq(4)
        end

        it 'returns 4 confirmation_messages when the sent datetime is equal both dates' do
          expect(ConfirmationMessage.filters(sent_from: DateTime.now.strftime('%Y-%m-%d'),
                                             sent_to: DateTime.now.strftime('%Y-%m-%d')).count).to eq(4)
        end

        it 'returns 0 confirmation_messages when the sent datetime is before both dates' do
          ConfirmationMessage.update_all(sent_datetime: (DateTime.now - 1.days) + (Time.now.utc_offset / 3600).hours)
          expect(ConfirmationMessage.filters(sent_from: DateTime.now.strftime('%Y-%m-%d'),
                                             sent_to: (DateTime.now + 1.days).strftime('%Y-%m-%d')).count).to eq(0)
        end

        it 'returns 0 confirmation_messages when the sent datetime is after both dates' do
          ConfirmationMessage.update_all(sent_datetime: (DateTime.now + 2.days) + (Time.now.utc_offset / 3600).hours)
          expect(ConfirmationMessage.filters(sent_from: DateTime.now.strftime('%Y-%m-%d'),
                                             sent_to: (DateTime.now + 1.days).strftime('%Y-%m-%d')).count).to eq(0)
        end
      end
    end
  end
end
