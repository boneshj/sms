require 'rails_helper'

describe SmsReminderService, type: :service do
  let!(:client) { create(:client) }
  let!(:mobile_phone) { create(:client_phone, client: client, number_type: 'Mobile', primary: true) }
  let!(:appointment) { create(:booking, client: client, location: client.location) }
  let!(:reminder_service) { SmsReminderService.new }

  describe '#remind' do
    before(:each) do
      SmsReminderService.send(:public, *SmsReminderService.private_instance_methods)
    end

    it 'should send sms' do
      reminder_service.remind(appointment)
      expect(FakeSMS.messages.last[:body]).to eq(reminder_service.msg_text(appointment))
    end

    it 'should not send sms if no mobile found' do
      appointment.client.client_phones.where(primary: true).first.update(number_type: 'Home')
      reminder_service.remind(appointment)
      expect(FakeSMS.messages.count).to eq(0)
    end

    it 'should send sms if mobile phone number found that starts with 04' do
      appointment.client.client_phones.where(primary: true).first.update(number: '0425123123')
      reminder_service.remind(appointment)
      expect(FakeSMS.messages.count).to eq(1)
    end

    it 'should create confirmation message record' do
      reminder_service.remind(appointment)
      expect(appointment.confirmation_message.message_text).to eq(reminder_service.msg_text(appointment))
      expect(appointment.confirmation_message.sent_datetime).not_to be_nil
    end
  end

  describe '#confirm_messages' do
    before(:each) do
      reminder_service.remind(appointment)
      allow_any_instance_of(SmsReminderService).to receive(:api_list_messages) { FakeSMS.messages }
    end

    context 'confirmed' do
      let(:params) { { 'From' => client.phone, 'Body' => 'YES' } }
      before :each do
        FakeSMS.confirmed(FakeSMS.messages)
        reminder_service.confirm_messages(DateTime.current.to_date - 1.days)
        appointment.reload
      end

      it 'should update Booking.confirmation_status' do
        expect(appointment.status).to eq('Confirmed')
      end

      it 'should update ConfirmationMessage.responses_received' do
        expect(appointment.confirmation_message.responses_received.upcase).to eq(params['Body'])
      end
    end

    context 'cancelled' do
      let(:params) { {'From' => client.phone, 'Body' => 'NO'}}
      before :each do
        FakeSMS.cancelled(FakeSMS.messages)
        reminder_service.confirm_messages(DateTime.current.to_date - 1.days)
        appointment.reload
      end

      it 'should update Booking.confirmation_status' do
        expect(appointment.status).to eq('Cancelled')
      end

      it 'should update ConfirmationMessage.responses_received' do
        expect(appointment.confirmation_message.responses_received.upcase).to eq(params['Body'])
      end
    end

    context 'different answer' do
      let(:params) { { 'From' => client.phone, 'Body' => 'Different answer' } }
      before :each do
        FakeSMS.garbage(FakeSMS.messages)
        reminder_service.confirm_messages(DateTime.current.to_date - 1.days)
        appointment.reload
      end

      it 'should not update Booking.confirmation_status' do
        expect(appointment.status).to eq('Booked')
      end

      it 'should update ConfirmationMessage.responses_received' do
        expect(appointment.confirmation_message.responses_received).to eq(params['Body'])
      end
    end
  end
end
