class FakeSMS
  Message = Struct.new(:from, :to, :body, :sid, :status)

  cattr_accessor :messages
  self.messages = []

  def initialize(account_sid, _auth_token)
  end

  def account
    self
  end

  def messages
    self
  end

  def create(opts)
    self.class.messages << Message.new(opts[:from], opts[:to], opts[:body], Faker::Crypto.sha1, 'sent')
  end

  def self.confirmed(messages)
    messages.each do |message|
      self.messages << Message.new(message[:to], message[:from], 'Yes', Faker::Crypto.sha1, 'received') if message[:status] == 'sent'
    end
  end

  def self.cancelled(messages)
    messages.each do |message|
      self.messages << Message.new(message[:to], message[:from], 'No', Faker::Crypto.sha1, 'received') if message[:status] == 'sent'
    end
  end

  def self.garbage(messages)
    messages.each do |message|
      self.messages << Message.new(message[:to], message[:from], 'Different answer', Faker::Crypto.sha1, 'received') if message[:status] == 'sent'
    end
  end
end
